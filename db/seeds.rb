# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Role.create!(role_type: "read_only")
Role.create!(role_type: "dt")
Role.create!(role_type: "mt")


User.create!(username: "May Myat Mon",
						email: "maymyatmon@gmail.com",
						password: "maymay",
						role_id: 1)


User.create!(username: "Hla Hla",
						email: "hlahla@gmail.com",
						password: "hlahla",
						role_id: 2)


User.create!(username: "Mg Mg",
						email: "mgmg@gmail.com",
						password: "mgmgmg",
						role_id: 3)

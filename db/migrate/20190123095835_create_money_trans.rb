class CreateMoneyTrans < ActiveRecord::Migration[5.2]
  def change
    create_table :money_trans do |t|
      t.date :date
      t.integer :qty
      t.decimal :price, precision: 30, scale: 6
      t.decimal :amount, precision: 30, scale: 6

      t.timestamps
    end
  end
end

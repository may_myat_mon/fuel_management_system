class AddUpdatedByToFuelTrans < ActiveRecord::Migration[5.2]
  def change
    add_column :fuel_trans, :updated_by, :integer
  end
end

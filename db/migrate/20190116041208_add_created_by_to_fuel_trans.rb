class AddCreatedByToFuelTrans < ActiveRecord::Migration[5.2]
  def change
    add_column :fuel_trans, :created_by, :integer
  end
end

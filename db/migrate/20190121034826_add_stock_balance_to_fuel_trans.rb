class AddStockBalanceToFuelTrans < ActiveRecord::Migration[5.2]
  def change
    add_column :fuel_trans, :stock_balance, :integer
  end
end

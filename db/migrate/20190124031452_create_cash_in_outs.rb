class CreateCashInOuts < ActiveRecord::Migration[5.2]
  def change
    create_table :cash_in_outs do |t|
      t.date :date
      t.decimal :amount,precision: 30, scale: 6

      t.timestamps
    end
  end
end

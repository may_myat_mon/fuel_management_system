class AddTransTypeToFuelTrans < ActiveRecord::Migration[5.2]
  def change
    add_column :fuel_trans, :trans_type, :string
  end
end

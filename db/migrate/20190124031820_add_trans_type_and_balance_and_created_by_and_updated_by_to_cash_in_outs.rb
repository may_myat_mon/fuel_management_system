class AddTransTypeAndBalanceAndCreatedByAndUpdatedByToCashInOuts < ActiveRecord::Migration[5.2]
  def change
    add_column :cash_in_outs, :trans_type, :string
    add_column :cash_in_outs, :balance, :decimal,precision: 30, scale: 6
    add_column :cash_in_outs, :created_by, :integer
    add_column :cash_in_outs, :updated_by, :integer
  end
end

class AddTransTypeAndCreatedByAndUpdatedByAndMoneyBalanceToMoneyTrans < ActiveRecord::Migration[5.2]
  def change
    add_column :money_trans, :trans_type, :string
    add_column :money_trans, :created_by, :integer
    add_column :money_trans, :updated_by, :integer
    add_column :money_trans, :money_balance, :integer
  end
end

Rails.application.routes.draw do
  
  resources :cash_in_outs
  resources :money_trans
  resources :fuel_trans
  resources :users

  root 'static_pages#home'
  get '/root', to: 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get  '/contact',    to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
	get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  get '/dashboards_fueltran', to: 'dashboards#fueltran'
  get '/dashboards_moneytran',to: 'dashboards#moneytran'
  get '/dashboards_cashinout',to: 'dashboards#cashinout'
  get '/maindashboards', to: 'dashboards#maindashboard'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

 



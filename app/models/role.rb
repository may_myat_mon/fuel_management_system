class Role < ApplicationRecord
	has_many :users , class_name: "User" , primary_key: "id" , foreign_key: "role_id"
end

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.role_id == 1
        can :read, :all
    elsif user.role_id == 2
        can :manage, :FuelTran
    else
        can :manage, :MoneyTran,:CashInOut 
    end
  end
end

class FuelTran < ApplicationRecord

	validates :date, presence: true
	validates :qty, presence: true
	validates :price, presence: true

	def self.multiply(qty,price)
		return qty * price
	end

	
end

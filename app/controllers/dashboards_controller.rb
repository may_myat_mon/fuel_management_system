class DashboardsController < ApplicationController
	before_action :fuel_gp, only: [:fueltran]
	before_action :money_gp, only: [:moneytran]
	before_action :fuel_gp,:money_gp, only: [:maindashboard]

  def fueltran
  	@fuel_trans = FuelTran.all
  	@fuel_gross_profit
  end

  def moneytran
  	@money_trans = MoneyTran.all
  	@money_gross_profit
  end

  def cashinout
  	@cash_in_outs = CashInOut.all
  end

  def maindashboard
  	@fuel_trans = FuelTran.all
  	@money_trans = MoneyTran.all
  	@cash_in_outs = CashInOut.all
  	@fuel_gross_profit
  	@money_gross_profit
  end

  private
  	def fuel_gp
      @fuel_trans = FuelTran.all
        @fuel_trans.each do |fuel_tran|
        @buy_qty = FuelTran.where(:trans_type => fuel_tran.trans_type ='01').sum(:qty)
        @buy_amount = FuelTran.where(:trans_type => fuel_tran.trans_type ='01').sum(:amount)
        @buy_unitprice = (@buy_amount / @buy_qty).round(4)

        @sale_qty = FuelTran.where(:trans_type => fuel_tran.trans_type ='02').sum(:qty)
        @sale_amount = FuelTran.where(:trans_type => fuel_tran.trans_type ='02').sum(:amount)
        @sale_unitprice = (@sale_amount / @sale_qty).round(4)

        @fuel_gross_profit = (@sale_unitprice * @sale_qty) - (@buy_unitprice * @buy_qty)
      end
    end

    def money_gp
      @money_trans = MoneyTran.all
        @money_trans.each do |money_tran|
        @buy_qty = MoneyTran.where(:trans_type => money_tran.trans_type ='01').sum(:qty)
        @buy_amount = MoneyTran.where(:trans_type => money_tran.trans_type ='01').sum(:amount)
        @buy_unitprice = (@buy_amount / @buy_qty).round(4)

        @sale_qty = MoneyTran.where(:trans_type => money_tran.trans_type ='02').sum(:qty)
        @sale_amount = MoneyTran.where(:trans_type => money_tran.trans_type ='02').sum(:amount)
        @sale_unitprice = (@sale_amount / @sale_qty).round(4)

        @money_gross_profit = (@sale_unitprice * @sale_qty) - (@buy_unitprice * @buy_qty)
      end
    end
end

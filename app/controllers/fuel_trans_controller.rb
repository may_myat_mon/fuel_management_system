class FuelTransController < ApplicationController

  before_action :set_fuel_tran, only: [:show, :edit, :update, :destroy]
  before_action :total_gp, only: [:index, :show]
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,:show]

  # GET /fuel_trans
  # GET /fuel_trans.json
  def index
    @fuel_trans = FuelTran.all
    @total_gross_profi 
  end

  # GET /fuel_trans/1
  # GET /fuel_trans/1.json
  def show
    @total_gross_profit
  end

  # GET /fuel_trans/new
  def new
    @fuel_tran = FuelTran.new 
  end

  # GET /fuel_trans/1/edit
  def edit
  end

  # POST /fuel_trans
  # POST /fuel_trans.json
  def create
    @fuel_tran = FuelTran.new(fuel_tran_params)
    @isChecked = params[:fuel_tran][:trans_type]
    if  @isChecked == 'true'
      @fuel_tran.trans_type = '01'
    else
      @fuel_tran.trans_type = '02'
    end
    @fuel_tran.created_by = current_user[:id]
    @fuel_tran.updated_by = current_user[:id]
    respond_to do |format|
      if @fuel_tran.save
        @fuel_trans = FuelTran.all
        @amount = FuelTran.multiply(@fuel_tran.qty,@fuel_tran.price)
        @fuel_trans.each do |fuel_tran|
          @buy_qty = FuelTran.where(:trans_type => fuel_tran.trans_type ='01').sum(:qty)
          @sale_qty = FuelTran.where(:trans_type => fuel_tran.trans_type ='02').sum(:qty)
          @stock_balance = @buy_qty - @sale_qty
          @fuel_tran.update_attributes(:stock_balance => @stock_balance,:amount => @amount)
        end
        format.html { redirect_to @fuel_tran, notice: 'Fuel transaction was successfully created.'}
        format.json { render :show,:index, status: :created, location: @fuel_tran }
      else
        format.html { render :new }
        format.json { render json: @fuel_tran.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fuel_trans/1
  # PATCH/PUT /fuel_trans/1.json
  def update 
      respond_to do |format|
      if @fuel_tran.update(fuel_tran_params)
        format.html { redirect_to @fuel_tran}
        format.json { render :show, status: :ok, location: @fuel_tran }
      else
        format.html { render :edit }
        format.json { render json: @fuel_tran.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fuel_trans/1
  # DELETE /fuel_trans/1.json
  def destroy
    @fuel_tran.destroy
    respond_to do |format|
      format.html { redirect_to fuel_trans_url, notice: 'Fuel tran was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fuel_tran
      @fuel_tran = FuelTran.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fuel_tran_params
      params.require(:fuel_tran).permit(:date, :qty, :price)
    end

    def total_gp
      @fuel_trans = FuelTran.all
        @fuel_trans.each do |fuel_tran|
        @buy_qty = FuelTran.where(:trans_type => fuel_tran.trans_type ='01').sum(:qty)
        @buy_amount = FuelTran.where(:trans_type => fuel_tran.trans_type ='01').sum(:amount)
        @buy_unitprice = @buy_amount / @buy_qty

        @sale_qty = FuelTran.where(:trans_type => fuel_tran.trans_type ='02').sum(:qty)
        @sale_amount = FuelTran.where(:trans_type => fuel_tran.trans_type ='02').sum(:amount)
        @sale_unitprice = @sale_amount / @sale_qty

        @total_gross_profit = (@sale_unitprice * @sale_qty) - (@buy_unitprice * @buy_qty)
      end
    end
end

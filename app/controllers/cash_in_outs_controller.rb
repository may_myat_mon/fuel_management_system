class CashInOutsController < ApplicationController
  before_action :set_cash_in_out, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]

  # GET /cash_in_outs
  # GET /cash_in_outs.json
  def index
    @cash_in_outs = CashInOut.all
  end

  # GET /cash_in_outs/1
  # GET /cash_in_outs/1.json
  def show
  end

  # GET /cash_in_outs/new
  def new
    @cash_in_out = CashInOut.new 
  end

  # GET /cash_in_outs/1/edit
  def edit
  end

  # POST /cash_in_outs
  # POST /cash_in_outs.json
  def create
    @cash_in = 0
    @cash_out = 0
    @cash_in_out = CashInOut.new(cash_in_out_params)
    @isChecked = params[:cash_in_out][:trans_type]
    if  @isChecked == 'true'
      @cash_in_out.trans_type = '01'
    else
      @cash_in_out.trans_type = '02'
    end
    @cash_in_out.created_by = current_user[:id]
    @cash_in_out.updated_by = current_user[:id]
    respond_to do |format|
      if @cash_in_out.save
        @cash_in_outs = CashInOut.all
        @cash_in_outs.each do |cash_in_out|
          @cash_in = CashInOut.where(:trans_type => cash_in_out.trans_type ='01').sum(:amount)
          @cash_out = CashInOut.where(:trans_type => cash_in_out.trans_type ='02').sum(:amount)
          @balance = @cash_in - @cash_out
          @cash_in_out.update_attributes( :balance => @balance )
      end
        format.html { redirect_to @cash_in_out, notice: 'Cash in out was successfully created.' }
        format.json { render :show, status: :created, location: @cash_in_out }
      else
        format.html { render :new }
        format.json { render json: @cash_in_out.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cash_in_outs/1
  # PATCH/PUT /cash_in_outs/1.json
  def update
    respond_to do |format|
      if @cash_in_out.update(cash_in_out_params)
        format.html { redirect_to @cash_in_out, notice: 'Cash in out was successfully updated.' }
        format.json { render :show, status: :ok, location: @cash_in_out }
      else
        format.html { render :edit }
        format.json { render json: @cash_in_out.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cash_in_outs/1
  # DELETE /cash_in_outs/1.json
  def destroy
    @cash_in_out.destroy
    respond_to do |format|
      format.html { redirect_to cash_in_outs_url, notice: 'Cash in out was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cash_in_out
      @cash_in_out = CashInOut.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cash_in_out_params
      params.require(:cash_in_out).permit(:date, :amount)
    end
end

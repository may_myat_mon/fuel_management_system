class MoneyTransController < ApplicationController
  before_action :set_money_tran, only: [:show, :edit, :update, :destroy]
  before_action :total_gp, only: [:index, :show]
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]

  # GET /money_trans
  # GET /money_trans.json
  def index
    @money_trans = MoneyTran.all
    @total_gross_profit 
  end

  # GET /money_trans/1
  # GET /money_trans/1.json
  def show
    @total_gross_profit
  end

  # GET /money_trans/new
  def new
    @money_tran = MoneyTran.new
  end

  # GET /money_trans/1/edit
  def edit
  end

  # POST /money_trans
  # POST /money_trans.json
  def create
    @money_tran = MoneyTran.new(money_tran_params)
    @isChecked = params[:money_tran][:trans_type]
    if  @isChecked == 'true'
      @money_tran.trans_type = '01'
    else
      @money_tran.trans_type = '02'
    end
    @money_tran.created_by = current_user[:id]
    @money_tran.updated_by = current_user[:id]
    respond_to do |format|
      if @money_tran.save
        @money_trans = MoneyTran.all
        @amount = MoneyTran.multiply(@money_tran.qty,@money_tran.price)
        @money_trans.each do |money_tran|
          @buy_qty = MoneyTran.where(:trans_type => money_tran.trans_type ='01').sum(:qty)
          @sale_qty = MoneyTran.where(:trans_type => money_tran.trans_type ='02').sum(:qty)
          @money_balance = @buy_qty - @sale_qty
          @money_tran.update_attributes(:money_balance => @money_balance,:amount => @amount)
        end
        format.html { redirect_to @money_tran, notice: 'Money tran was successfully created.' }
        format.json { render :show, status: :created, location: @money_tran }
      else
        format.html { render :new }
        format.json { render json: @money_tran.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /money_trans/1
  # PATCH/PUT /money_trans/1.json
  def update
    respond_to do |format|
      if @money_tran.update(money_tran_params)
        format.html { redirect_to @money_tran, notice: 'Money tran was successfully updated.' }
        format.json { render :show, status: :ok, location: @money_tran }
      else
        format.html { render :edit }
        format.json { render json: @money_tran.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /money_trans/1
  # DELETE /money_trans/1.json
  def destroy
    @money_tran.destroy
    respond_to do |format|
      format.html { redirect_to money_trans_url, notice: 'Money tran was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_money_tran
      @money_tran = MoneyTran.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def money_tran_params
      params.require(:money_tran).permit(:date, :qty, :price, :amount)
    end

    def total_gp
      @money_trans = MoneyTran.all
        @money_trans.each do |money_tran|
        @buy_qty = MoneyTran.where(:trans_type => money_tran.trans_type ='01').sum(:qty)
        @buy_amount = MoneyTran.where(:trans_type => money_tran.trans_type ='01').sum(:amount)
        @buy_unitprice = (@buy_amount / @buy_qty).round(4)

        @sale_qty = MoneyTran.where(:trans_type => money_tran.trans_type ='02').sum(:qty)
        @sale_amount = MoneyTran.where(:trans_type => money_tran.trans_type ='02').sum(:amount)
        @sale_unitprice = (@sale_amount / @sale_qty).round(4)

        @total_gross_profit = (@sale_unitprice * @sale_qty) - (@buy_unitprice * @buy_qty)
      end
    end
end

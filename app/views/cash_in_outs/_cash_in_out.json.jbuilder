json.extract! cash_in_out, :id, :date, :amount, :created_at, :updated_at
json.url cash_in_out_url(cash_in_out, format: :json)

json.extract! money_tran, :id, :date, :qty, :price, :amount, :created_at, :updated_at
json.url money_tran_url(money_tran, format: :json)

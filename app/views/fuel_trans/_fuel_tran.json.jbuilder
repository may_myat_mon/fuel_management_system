json.extract! fuel_tran, :id, :date, :qty, :unit_price, :total_price, :created_at, :updated_at
json.url fuel_tran_url(fuel_tran, format: :json)

require 'test_helper'

class CashInOutsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cash_in_out = cash_in_outs(:one)
  end

  test "should get index" do
    get cash_in_outs_url
    assert_response :success
  end

  test "should get new" do
    get new_cash_in_out_url
    assert_response :success
  end

  test "should create cash_in_out" do
    assert_difference('CashInOut.count') do
      post cash_in_outs_url, params: { cash_in_out: { amount: @cash_in_out.amount, date: @cash_in_out.date } }
    end

    assert_redirected_to cash_in_out_url(CashInOut.last)
  end

  test "should show cash_in_out" do
    get cash_in_out_url(@cash_in_out)
    assert_response :success
  end

  test "should get edit" do
    get edit_cash_in_out_url(@cash_in_out)
    assert_response :success
  end

  test "should update cash_in_out" do
    patch cash_in_out_url(@cash_in_out), params: { cash_in_out: { amount: @cash_in_out.amount, date: @cash_in_out.date } }
    assert_redirected_to cash_in_out_url(@cash_in_out)
  end

  test "should destroy cash_in_out" do
    assert_difference('CashInOut.count', -1) do
      delete cash_in_out_url(@cash_in_out)
    end

    assert_redirected_to cash_in_outs_url
  end
end

require 'test_helper'

class MoneyTransControllerTest < ActionDispatch::IntegrationTest
  setup do
    @money_tran = money_trans(:one)
  end

  test "should get index" do
    get money_trans_url
    assert_response :success
  end

  test "should get new" do
    get new_money_tran_url
    assert_response :success
  end

  test "should create money_tran" do
    assert_difference('MoneyTran.count') do
      post money_trans_url, params: { money_tran: { amount: @money_tran.amount, date: @money_tran.date, price: @money_tran.price, qty: @money_tran.qty } }
    end

    assert_redirected_to money_tran_url(MoneyTran.last)
  end

  test "should show money_tran" do
    get money_tran_url(@money_tran)
    assert_response :success
  end

  test "should get edit" do
    get edit_money_tran_url(@money_tran)
    assert_response :success
  end

  test "should update money_tran" do
    patch money_tran_url(@money_tran), params: { money_tran: { amount: @money_tran.amount, date: @money_tran.date, price: @money_tran.price, qty: @money_tran.qty } }
    assert_redirected_to money_tran_url(@money_tran)
  end

  test "should destroy money_tran" do
    assert_difference('MoneyTran.count', -1) do
      delete money_tran_url(@money_tran)
    end

    assert_redirected_to money_trans_url
  end
end

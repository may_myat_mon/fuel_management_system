require 'test_helper'

class FuelTransControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fuel_tran = fuel_trans(:one)
  end

  test "should get index" do
    get fuel_trans_url
    assert_response :success
  end

  test "should get new" do
    get new_fuel_tran_url
    assert_response :success
  end

  test "should create fuel_tran" do
    assert_difference('FuelTran.count') do
      post fuel_trans_url, params: { fuel_tran: { date: @fuel_tran.date, qty: @fuel_tran.qty, total_price: @fuel_tran.total_price, unit_price: @fuel_tran.unit_price } }
    end

    assert_redirected_to fuel_tran_url(FuelTran.last)
  end

  test "should show fuel_tran" do
    get fuel_tran_url(@fuel_tran)
    assert_response :success
  end

  test "should get edit" do
    get edit_fuel_tran_url(@fuel_tran)
    assert_response :success
  end

  test "should update fuel_tran" do
    patch fuel_tran_url(@fuel_tran), params: { fuel_tran: { date: @fuel_tran.date, qty: @fuel_tran.qty, total_price: @fuel_tran.total_price, unit_price: @fuel_tran.unit_price } }
    assert_redirected_to fuel_tran_url(@fuel_tran)
  end

  test "should destroy fuel_tran" do
    assert_difference('FuelTran.count', -1) do
      delete fuel_tran_url(@fuel_tran)
    end

    assert_redirected_to fuel_trans_url
  end
end

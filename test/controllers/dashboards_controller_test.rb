require 'test_helper'

class DashboardsControllerTest < ActionDispatch::IntegrationTest
  test "should get fueltran" do
    get dashboards_fueltran_url
    assert_response :success
  end

  test "should get moneytran" do
    get dashboards_moneytran_url
    assert_response :success
  end

  test "should get cashinout" do
    get dashboards_cashinout_url
    assert_response :success
  end

end

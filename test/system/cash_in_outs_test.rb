require "application_system_test_case"

class CashInOutsTest < ApplicationSystemTestCase
  setup do
    @cash_in_out = cash_in_outs(:one)
  end

  test "visiting the index" do
    visit cash_in_outs_url
    assert_selector "h1", text: "Cash In Outs"
  end

  test "creating a Cash in out" do
    visit cash_in_outs_url
    click_on "New Cash In Out"

    fill_in "Amount", with: @cash_in_out.amount
    fill_in "Date", with: @cash_in_out.date
    click_on "Create Cash in out"

    assert_text "Cash in out was successfully created"
    click_on "Back"
  end

  test "updating a Cash in out" do
    visit cash_in_outs_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @cash_in_out.amount
    fill_in "Date", with: @cash_in_out.date
    click_on "Update Cash in out"

    assert_text "Cash in out was successfully updated"
    click_on "Back"
  end

  test "destroying a Cash in out" do
    visit cash_in_outs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cash in out was successfully destroyed"
  end
end

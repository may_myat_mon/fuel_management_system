require "application_system_test_case"

class FuelTransTest < ApplicationSystemTestCase
  setup do
    @fuel_tran = fuel_trans(:one)
  end

  test "visiting the index" do
    visit fuel_trans_url
    assert_selector "h1", text: "Fuel Trans"
  end

  test "creating a Fuel tran" do
    visit fuel_trans_url
    click_on "New Fuel Tran"

    fill_in "Date", with: @fuel_tran.date
    fill_in "Qty", with: @fuel_tran.qty
    fill_in "Total price", with: @fuel_tran.total_price
    fill_in "Unit price", with: @fuel_tran.unit_price
    click_on "Create Fuel tran"

    assert_text "Fuel tran was successfully created"
    click_on "Back"
  end

  test "updating a Fuel tran" do
    visit fuel_trans_url
    click_on "Edit", match: :first

    fill_in "Date", with: @fuel_tran.date
    fill_in "Qty", with: @fuel_tran.qty
    fill_in "Total price", with: @fuel_tran.total_price
    fill_in "Unit price", with: @fuel_tran.unit_price
    click_on "Update Fuel tran"

    assert_text "Fuel tran was successfully updated"
    click_on "Back"
  end

  test "destroying a Fuel tran" do
    visit fuel_trans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Fuel tran was successfully destroyed"
  end
end

require "application_system_test_case"

class MoneyTransTest < ApplicationSystemTestCase
  setup do
    @money_tran = money_trans(:one)
  end

  test "visiting the index" do
    visit money_trans_url
    assert_selector "h1", text: "Money Trans"
  end

  test "creating a Money tran" do
    visit money_trans_url
    click_on "New Money Tran"

    fill_in "Amount", with: @money_tran.amount
    fill_in "Date", with: @money_tran.date
    fill_in "Price", with: @money_tran.price
    fill_in "Qty", with: @money_tran.qty
    click_on "Create Money tran"

    assert_text "Money tran was successfully created"
    click_on "Back"
  end

  test "updating a Money tran" do
    visit money_trans_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @money_tran.amount
    fill_in "Date", with: @money_tran.date
    fill_in "Price", with: @money_tran.price
    fill_in "Qty", with: @money_tran.qty
    click_on "Update Money tran"

    assert_text "Money tran was successfully updated"
    click_on "Back"
  end

  test "destroying a Money tran" do
    visit money_trans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Money tran was successfully destroyed"
  end
end
